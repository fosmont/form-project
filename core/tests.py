from django.test import TestCase
from core.models import Animal

# Create your tests here.
class AnimalsTests(TestCase):
	def test_create(self):
		lion = Animal.objects.create(name="Lion")
		self.assertEqual(lion.name, "Lion")
